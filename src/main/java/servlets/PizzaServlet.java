package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class PizzaServlet extends HttpServlet {

    private static Map<Integer, Pizza> pizzas = new HashMap<>();
    public void init() throws ServletException {
        Pizza p1 = new Pizza("Carbonarra");
        Pizza p2 = new Pizza("Provans");
        Pizza p3 = new Pizza("4 Cheeseeseseses");
        Pizza p4 = new Pizza("quatro stadzhoni");
        Pizza p5 = new Pizza("President");
        pizzas.put(p1.getID(), p1);
        pizzas.put(p2.getID(), p2);
        pizzas.put(p3.getID(), p3);
        pizzas.put(p4.getID(), p4);
        pizzas.put(p5.getID(), p5);
    };

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<form action='hello' method='POST'>"
        + "ToServer:<input type='text' name='info'> <button type='submit'> Send to server </button> </form>");
        out.println("<p>req URI: "+ req.getRequestURI() + "</p>");
        out.println("</body></html>");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
        doGet(req, resp);
    }

    public void destroy(){}
}
