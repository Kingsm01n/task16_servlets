package servlets;

public class Pizza {
    private static int unique = 1;
    private int ID;
    private String name;

    public Pizza(String name){
        this.name = name;
        ID = unique;
        unique++;
    }

    public static int getUnique() {
        return unique;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public static void setUnique(int unique) {
        Pizza.unique = unique;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }
}
